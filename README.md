# slides
The workshop slides

- Install Atom to edit the readme files
- To see the slides start a http server with root dir the slides directory. For example:
```
cd slides
docker run -d -p 80:80 --name slides \
  -v ${PWD}:/usr/share/nginx/html nginx
```
- Open a browser [slides](http://localhost/)

# starter-docker
Sources for the docker starters

# starter-gradle
Sources for the gradle starters

# continuous
Sources for the main course
