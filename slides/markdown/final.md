# Closing notes


# Reference material

- Slides are available as Docker image on Docker Hub, [Slides](https://hub.docker.com/r/atosjava/continuousfunuk/)
- All sources used for the handson lab are available on BitBucket, [GIT](https://bitbucket.org/devopsuk/workshop)
- DIY: Setup a (VM) machine running Docker 1.9.0 and Docker Compose 1.5.2

